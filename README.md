 **Alibaba-Cloud** 一个简洁美观、功能强大组件学习。使用springboot+AlibabaCloud开发    

![JDK](https://img.shields.io/badge/JDK-1.8-green.svg)
![Maven](https://img.shields.io/badge/Maven-3.3.1-green.svg)

----

# 重要声明

**看文档！看文档！看文档！**，初次使用， 请先**看文档！**

 文档地址：<a href="https://blog.csdn.net/qq_42082701/article/details/100983913">点我读取文档</a>


----

# 写在前面的话
ps: 虽然我知道，大部分人都是来了**直接下载源代码**后就潇洒的离开，并且只有等到下次**突然想到**“我天~~我得去看看Alibaba-Cloud这烂项目更新新功能了吗”的时候才会重新来到这儿，即使你重新来过，我估计你也只有两个选择：    

发现更新代码了 --> 下载源码后重复上面的步骤    
发现没更新代码 --> 直接关闭浏览器

虽然我知道现实就是如此的残酷，但我还是要以我萤虫之力对各位到来的同仁发出一声诚挚的嘶吼：

**如果喜欢，请多多分享！！多多Star！！**

----


# 功能简介

该demo是 Springboot+AlibabaCloud 搭建的
1、nacos 注册中心
2、sentinel 流量控制，断路、限流的使用
3、apache-skywalking-apm-bin 链路跟踪
4、Gateway 网关
5、Fegin调用
6、ES搭建
7、yml的方式、默认是 properties 使用


# 技术栈
该demo是 Springboot+AlibabaCloud 搭建的
1、nacos 注册中心
2、sentinel 流量控制，断路、限流的使用
3、apache-skywalking-apm-bin 链路跟踪
4、Gateway 网关
5、Fegin调用
6、ES搭建
7、yml的方式、默认是 properties 使用

- ...


# 后续扩展


# 交流

|  (备注:QQ加群)  |  公众号  |
| :------------: | :------------: |
| <img src="https://img-blog.csdnimg.cn/20191103182851996.png" width="170"/> | <img src="https://www.e404e.cn/wp-content/uploads/2021/02/QQ%E5%9B%BE%E7%89%8720200918103453.jpg" width="200" /> |



# 生命不息，折腾不止！ 更多信息，请关注：
 1. [我的博客](https://blog.csdn.net/qq_42082701?spm=1011.2124.3001.5343)
 2. [我的网站](https://www.e404e.cn)
 
# 特别感谢

# 开源协议

[![license](https://gitee.com/yswyn_admin/projects)](https://gitee.com/yswyn_admin/projects)
